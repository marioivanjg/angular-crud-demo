import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Alumno } from '../models/alumno';

@Injectable({
  providedIn: 'root'
})
export class AlumnoService {

  private baseEndpoint = 'http://localhost:8080/demo/api';

  private cabeceras: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  public listar(): Observable<Alumno[]> {
    return this.http.get<Alumno[]>(`${this.baseEndpoint}/getUsers`);
  }

  public listarPaginas(page: string, size: string): Observable<any> {
    const params1 = new HttpParams()
      .set('page', page)
      .set('size', size);
    return this.http.get<any>(`${this.baseEndpoint}/getUsers/pagina`, {params : params1});
  }

  public ver(id: number): Observable<Alumno> {
    return this.http.get<Alumno>(`${this.baseEndpoint}/getUser?userKey=${id}`);
  }

  public crear(alumno: Alumno): Observable<Alumno> {
    return this.http.post<Alumno>(`${this.baseEndpoint}/saveUser`, alumno,
      { headers: this.cabeceras });
  }

  public editar(alumno: Alumno): Observable<Alumno> {
    return this.http.post<Alumno>(`${this.baseEndpoint}/updateUser`, alumno,
      { headers: this.cabeceras });
  }

  public eliminar(id: number): Observable<void> {
    return this.http.get<void>(`${this.baseEndpoint}/removeUser?userKey=${id}`);
  }
}
