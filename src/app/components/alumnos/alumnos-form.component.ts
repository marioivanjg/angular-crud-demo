import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { Alumno } from 'src/app/models/alumno';
import { AlumnoService } from 'src/app/services/alumno.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-alumnos-form',
  templateUrl: './alumnos-form.component.html',
  styleUrls: ['./alumnos-form.component.css']
})
export class AlumnosFormComponent implements OnInit {

  titulo = 'Crear Usuario';

  alumno: Alumno = new Alumno();

  error: any;

  constructor(private service: AlumnoService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const id: number = +params.get('id');
      if (id) {
        // Si se obtiene el id de un Alumno, consultamos el Alumno y lo asignamos al Modelo.
        this.service.ver(id).subscribe(alumno => this.alumno = alumno);
      }
    });
  }

  public crear(): void {
    this.alumno.userKey = this.alumno.userId;
    this.service.crear(this.alumno).subscribe(alumno => {
      console.log(alumno);
      Swal.fire('Nuevo:', `Usuario ${alumno.userName} creado con éxito!`, 'success');
      this.router.navigate(['/usuarios']);
    }, err => {
      if (err.status === 400) {
        this.error = err.error;
        console.log(this.error);
      }
    });
  }

  public editar(): void {
    this.service.editar(this.alumno).subscribe(alumno => {
      console.log(alumno);
      Swal.fire('Usuario Modificado!: ', `Usuario ${alumno.userName} actualizado con éxito`, 'success');
      this.router.navigate(['/usuarios']);
    }, err => {
      if (err.status === 400) {
        this.error = err.error;
        console.log(this.error);
      }
    });
  }
}
