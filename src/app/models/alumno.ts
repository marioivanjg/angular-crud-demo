export class Alumno {
    userKey: number;
    source: number;
    userId: number;
    userName: string;
    userPBXId: string;
    userEmail: string;
    userNtLogin: string;
    tenantKey: number;
    orgKey: number;
}
